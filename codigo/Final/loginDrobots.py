#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Sergio Perez Sanchez, Marcos Sanchez Iglesias, Francisco Murillo Lopez


import sys
import Ice
from random import randrange

Ice.loadSlice('drobots.ice')
import drobots

from RobotController import createFactory

class PlayerI(drobots.Player):
    def __init__(self, identificador):
        self.id = identificador
        self.salidaError = None

    def makeController(self, bot, current=None):
        return createFactory.createRobotController(self, bot, current.adapter)

    def win(self, current=None):
        self.salidaError = 0
        print("Ganas la partida")
        current.adapter.getCommunicator().shutdown()

    def lose(self, current=None):
        self.salidaError = 1
        print("Todos los robots han sido eliminados")
        current.adapter.getCommunicator().shutdown()


class loginDrobots(Ice.Application):
    def run(self, args):
        # Creamos el adaptador
        broker = self.communicator()
        adaptador = broker.createObjectAdapter("Adaptador")
        adaptador.activate()
        # Creamos el sirviente y lo añadimos al adaptador
        nickRandom = randrange(90000)
        nick = str(nickRandom) + "r"
        sirvienteJugador = PlayerI(nick)
        proxyJugador = adaptador.add(sirvienteJugador, broker.stringToIdentity("jugador20"))
        print("Proxy Jugador: " + str(proxyJugador))

        dirGame = args[1]
        proxyGame = broker.stringToProxy(dirGame)
        print("Proxy Servidor Game: " + str(proxyGame))

        player = drobots.PlayerPrx.uncheckedCast(proxyJugador)
        game = drobots.GamePrx.checkedCast(proxyGame)

        if not game:
            raise RuntimeError("Proxy error")
        try:
            nickRandom = randrange(1000)
            nick = str(nickRandom) + "r"
            print("nick: " + nick)
            game.login(player, nick)
            print("Comienza la partida")
        except drobots.InvalidProxy:
            print("Proxy no valido")
            return 1
        except drobots.InvalidName:
            print("ID no valido")
            return 1
        except drobots.GameInProgress:
            print("ID no valido")
            return 1

        sys.stdout.flush()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return sirvienteJugador.salidaError

sys.exit(loginDrobots().main(sys.argv))
