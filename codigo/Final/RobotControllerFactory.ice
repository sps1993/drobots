module RobotControllerFactory {
    interface factory {
        void createRobotController();
    };
};
