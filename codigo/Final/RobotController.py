import sys
import Ice
Ice.loadSlice('drobots.ice')
import drobots
Ice.loadSlice('RobotControllerFactory.ice')
import RobotControllerFactory
import random

#Sergio Perez Sanchez, Marcos Sanchez Iglesias, Francisco Murillo Lopez

class createFactory(RobotControllerFactory.factory):

    def __init__(self):
        self.hola="2"

    def createRobotController(self, bot, adaptador, current = None,):
        sirviente = RobotControllerI(bot)
        proxyrc = adaptador.addWithUUID(sirviente)
        robot_controller = drobots.RobotControllerPrx.uncheckedCast(proxyrc)
        return robot_controller

class RobotControllerI(drobots.RobotController):
    def __init__(self, robot):
        self.robot = robot
        self.nRobots = 0
        self.angulo=0
        self.velo=0

    def turn(self, current=None):
        x=self.robot.location().x
        y=self.robot.location().y
        print("Localizacion --> x: " + str(x) + " e y: " + str(y))
        if self.robot.ice_isA("::drobots::Attacker"):
            if random.randint(0, 2)==1:
                self.robot.cannon(random.randint(0, 360),random.randint(0, 400))
                self.velo=0
            else:
                if self.velo==0:
                    self.robot.drive(random.randint(0, 360), random.randint(0, 101))
                    self.velo=1

        else:
            if random.randint(0, 2) == 1:
                self.angulo=random.randint(0, 360)
                self.nRobots=self.robot.scan(self.angulo, random.randint(1, 21))
                self.velo=0
            else:
                if self.nRobots>=1:
                    if self.velo==0:
                        self.robot.drive(random.randint(self.angulo, 360), random.randint(0, 101))
                        self.velo=1
                else:
                    if self.velo==0:
                        self.robot.drive(random.randint(0, 360), random.randint(0, 101))
                        self.velo=1

    def robotDestroyed(self, current=None):
        print ("Robot destruido")
