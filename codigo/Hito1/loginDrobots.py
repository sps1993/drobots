#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import Ice
from random import randrange
Ice.loadSlice('drobots.ice')
import drobots


class PlayerI(drobots.Player):

    def __init__(self, identificador):
        self.id=identificador
        self.salidaError = None

    def makeController(self, bot, current=None):
        print("llega")
        broker = self.communicator()
        adaptador = broker.createObjectAdapter("Adaptador2")
        sirviente = RobotControllerI(bot)
        proxy = self.adapter.add(sirviente, self.broker.stringToIdentity("robotController1"))
        robot_controller = drobots.RobotControllerPrx.uncheckedCast(proxy)
        print("Robot Controller: "+str(robot_controller))
        print("Hola")
        return robot_controller

    def win(self, current=None):
        self.salidaError = 0
        print("Login superado")
        current.adapter.getCommunicator().shutdown()
    
    def lose(self, current=None):
        self.salidaError = 1
        print("Login no superado")
        current.adapter.getCommunicator().shutdown()


class loginDrobots(Ice.Application):
    
    def run(self, args):        
        #Creamos el adaptador
        broker = self.communicator()
        adaptador = broker.createObjectAdapter("Adaptador")
        adaptador.activate()
        #Creamos el sirviente y lo añadimos al adaptador
        sirvienteJugador = PlayerI("03934534q")
        proxyJugador = adaptador.add(sirvienteJugador, broker.stringToIdentity("jugador20"))
        print("Proxy Jugador: "+str(proxyJugador))
        
        dirGame = args[1]
        proxyGame = broker.stringToProxy(dirGame)  
        print("Proxy Servidor Game: "+str(proxyGame))

        player = drobots.PlayerPrx.uncheckedCast(proxyJugador)   
        game = drobots.GamePrx.checkedCast(proxyGame)
        
        if not game:
            raise RuntimeError("Proxy error")
        try:
            nickRandom = randrange(10)
            nick = str(nickRandom)
            game.login(player, nick)
            print("Comienza la partida")
        except drobots.InvalidProxy:
            print("Proxy no valido")
            return 1
        except drobots.InvalidName:
            print("ID no valido")
            return 1

        sys.stdout.flush()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return sirvienteJugador.salidaError


class RobotControllerI(drobots.RobotController):

    def __init__(self, robot):        
        self.robot=robot

    def turn(self, current=None):
        print("Localizacion --> x: "+str(self.robot.location().x)+" e y: "+str(self.robot.location().y))

    def robotDestroyed(self, current=None):
        print ("Robot destruido")
        print()

sys.exit(loginDrobots().main(sys.argv))
