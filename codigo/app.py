#!/usr/bin/python3 -u

import sys
import math
import Ice
Ice.loadSlice("drobots.ice")
import drobots


class RobotControllerI(drobots.RobotController):
    def __init__(self, bot):
        self.bot=bot

    def turn(self, current):
        dest = drobots.Point(499, 499)
        pos = self.bot.location()
        print("My turn: position {0}".format(pos))

        angle = math.atan2(dest[1] - pos.y, dest [0] - pos.x)
        print("Drive to {0}".format(angle))
        self.bot.drive(angle, 100)

    def robotDestroyed(self, current):
        print("Oh oh")


class PlayerI(drobots.Player):
    def __init__(self):
        self.status=-1

    def makeController(self, bot, current):
        servant = RobotControllerI(bot)
        servant_prx = drobots.RobotControllerPrx.uncheckedCast(current.adapter.addWithUUID(servant))
        return servant_prx
    
    def win(self, current):
        print("I win!")
        current.adapter.getCommunicator().shutdown()
        self.status=0

    def lose(self, current):
        print("I lose!")
        current.adapter.getCommunicator().shutdown()
        self.status=1

    def gameAbort(self, current):
        print("Something happened")
        current.adapter.getCommunicator().shutdown()
        self.status=2

class App(Ice.Application):
    def run(self, argv):
        broker=self.communicator()

        adapter = broker.createObjectAdapter("PlayerAdapter")
        adapter.activate()

        prx = broker.stringToProxy(argv[1])
        game_prx = drobots.GamePrx.checkedCast(prx)

        servant = PlayerI()
        servant_prx = drobots.PlayerPrx.uncheckedCast(adapter.addWithUUID(servant))

        try:
            game_prx.login(servant_prx, "marcos")

        except Exception as e:
            print(e)
            return 3
  
        self.shutdownOnInterrupt()
        broker.waitForShutdown()
        #adapter.deactivate()

        return servant.status

app = App()
sys.exit(app.main(sys.argv))
