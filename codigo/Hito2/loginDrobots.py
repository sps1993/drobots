#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Autores:
    Francisco Murillo López
    Sergio Pérez Sánchez
    Marcos Sánchez Iglesias
'''

import sys
import Ice
Ice.loadSlice('drobots.ice')
import drobots


class PlayerI(drobots.Player):

    def __init__(self, identificador):
        self.id=identificador
        self.salidaError = None

    def win(self, current=None):
        self.salidaError = 0
        print("Login superado")
        current.adapter.getCommunicator().shutdown()
    
    def lose(self, current=None):
        self.salidaError = 1
        print("Login no superado")
        current.adapter.getCommunicator().shutdown()


class loginDrobots(Ice.Application):
    
    def run(self, args):        
        #Creamos el adaptador
        broker = self.communicator()
        adaptador = broker.createObjectAdapter("Adaptador")
        adaptador.activate()
        #Creamos el sirviente y lo añadimos al adaptador
        sirvienteJugador = PlayerI("03934534q")
        proxyJugador = adaptador.add(sirvienteJugador, broker.stringToIdentity("jugador1"))
        print("Proxy Jugador: "+str(proxyJugador))
        
        dirGame = args[1]
        proxyGame = broker.stringToProxy(dirGame)  
        print("Proxy Servidor Game: "+str(proxyGame))

        player = drobots.PlayerPrx.uncheckedCast(proxyJugador)   
        game = drobots.GamePrx.checkedCast(proxyGame)
        
        if not game:
            raise RuntimeError("Proxy error")
        try:
            nick="MarcosSergioPaco"
            game.login(player, nick)
        except drobots.InvalidProxy:
            print("Proxy no valido")
            return 1
        except drobots.InvalidName:
             print("ID no valido")
             return 1

        sys.stdout.flush()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return sirvienteJugador.salidaError


sys.exit(loginDrobots().main(sys.argv))
